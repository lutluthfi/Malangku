package com.arsldev.lutluthfi.mvpboilerplate.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

public final class ImageUtils {

    private ImageUtils() {
        // This utility is not publicly instantiable
    }

    public static void loadImage(Context context, String resource, ImageView holder) {
        Glide.with(context).load(resource).asBitmap().centerCrop().into(holder);
    }

    public static void loadImage(Context context, int resource, ImageView holder) {
        Glide.with(context).load(resource).asBitmap().centerCrop().into(holder);
    }

    public static BitmapImageViewTarget roundedImageTarget(Context context, ImageView imageView, float radius) {
        return new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                super.setResource(resource);
                RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                drawable.setCornerRadius(radius);
                imageView.setImageDrawable(drawable);
            }
        };
    }

    // TODO : not provide for custom target
}
