package com.arsldev.lutluthfi.malangku.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.base.BaseActivity;
import com.arsldev.lutluthfi.malangku.ui.main.home.HomeFragment;
import com.arsldev.lutluthfi.malangku.ui.main.mypoints.MyPointsFragment;
import com.arsldev.lutluthfi.malangku.ui.main.myprofile.MyProfileFragment;
import com.arsldev.lutluthfi.malangku.ui.main.notifications.NotificationsFragment;
import com.arsldev.lutluthfi.mvpboilerplate.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.navigation) BottomNavigationView mNavigation;

    public static Intent startIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUnBinder(ButterKnife.bind(this));
        mNavigation.setOnNavigationItemSelectedListener(this);
        setupView();
    }

    @Override
    public void setSupportActionBar(@Nullable Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
        if (toolbar != null) toolbar.setContentInsetStartWithNavigation(0);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                changeFragment(HomeFragment.newInstance(), HomeFragment.TAG);
                setTitle(getString(R.string.home));
                return true;
            case R.id.navigation_notifications:
                changeFragment(NotificationsFragment.newInstance(), NotificationsFragment.TAG);
                setTitle(getString(R.string.notifications));
                return true;
            case R.id.navigation_points:
                changeFragment(MyPointsFragment.newInstance(), MyPointsFragment.TAG);
                setTitle(getString(R.string.my_points));
                return true;
            case R.id.navigation_profile:
                changeFragment(MyProfileFragment.newInstance(), MyProfileFragment.TAG);
                setTitle(getString(R.string.my_profile));
                return true;
        }
        return false;
    }

    @Override
    public void setupView() {
        setSupportActionBar(mToolbar);
        CommonUtils.shiftingBottomNavigation(mNavigation);
        setTitle(getString(R.string.home));
        changeFragment(HomeFragment.newInstance(), HomeFragment.TAG);
    }

    private void changeFragment(Fragment fragment, String tag) {
        if (fragment != null ) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, fragment, tag).commit();
        } else {
            Toast.makeText(this, R.string.error_general, Toast.LENGTH_SHORT).show();
        }
    }
}
