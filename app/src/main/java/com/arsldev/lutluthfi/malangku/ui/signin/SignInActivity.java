package com.arsldev.lutluthfi.malangku.ui.signin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.base.BaseActivity;
import com.arsldev.lutluthfi.malangku.ui.main.MainActivity;
import com.arsldev.lutluthfi.malangku.ui.preference.PreferenceActivity;
import com.arsldev.lutluthfi.malangku.ui.prefix.PrefixActivity;
import com.arsldev.lutluthfi.malangku.ui.signup.SignUpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignInActivity extends BaseActivity {

    public static final String TAG = SignInActivity.class.getSimpleName();

    @BindView(R.id.edittext_sign_in_email) EditText mEmailEditText;
    @BindView(R.id.edittext_sign_in_password) EditText mPasswordEditText;
    @BindView(R.id.button_sign_in) Button mSignInButton;
    @BindView(R.id.textview_sign_up) TextView mSignUpTextView;

    public static Intent startIntent(Context context) {
        return new Intent(context, SignInActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        setUnBinder(ButterKnife.bind(this));
        setupView();
    }

    @Override
    public void setupView() {

    }

    public void onSignInClick(View view) {
        startActivity(new Intent(this, PreferenceActivity.class));
        finish();
    }

    public void onSignUpClick(View view) {
        startActivity(new Intent(this, SignUpActivity.class));
        finish();
    }
}
