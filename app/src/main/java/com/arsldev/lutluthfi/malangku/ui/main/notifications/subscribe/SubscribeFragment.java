package com.arsldev.lutluthfi.malangku.ui.main.notifications.subscribe;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.adapter.SubscribeAdapter;
import com.arsldev.lutluthfi.malangku.ui.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubscribeFragment extends BaseFragment implements SubscribeAdapter.OnItemClickListener<String>{

    public static final String TAG = SubscribeFragment.class.getSimpleName();

    @BindView(R.id.container_subscribe) RecyclerView mRecyclerView;

    private SubscribeAdapter mAdapter;

    public static SubscribeFragment newInstance() {
        SubscribeFragment fragment = new SubscribeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subscribe, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        return view;
    }

    @Override
    public void setupView(View view) {
        mAdapter = new SubscribeAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setSmoothScrollbarEnabled(true);
        mRecyclerView.setHasFixedSize(true);
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        List<String> contents = new ArrayList<>();
        contents.add("21 Pengembang yang tergabung dalam Real Estate Indonesia (REI) DKI Jakarta menandatangani dukungan program DP 0 persen yang dicanangkan Pemprov DKI. Wakil Gubernur DKI Jakarta Sandiaga Uno menyambut dukungan itu.");
        contents.add("21 Pengembang yang tergabung dalam Real Estate Indonesia (REI) DKI Jakarta menandatangani dukungan program DP 0 persen yang dicanangkan Pemprov DKI. Wakil Gubernur DKI Jakarta Sandiaga Uno menyambut dukungan itu.");
        contents.add("21 Pengembang yang tergabung dalam Real Estate Indonesia (REI) DKI Jakarta menandatangani dukungan program DP 0 persen yang dicanangkan Pemprov DKI. Wakil Gubernur DKI Jakarta Sandiaga Uno menyambut dukungan itu.");
        contents.add("21 Pengembang yang tergabung dalam Real Estate Indonesia (REI) DKI Jakarta menandatangani dukungan program DP 0 persen yang dicanangkan Pemprov DKI. Wakil Gubernur DKI Jakarta Sandiaga Uno menyambut dukungan itu.");
        mAdapter.setItems(contents);
    }

    @Override
    public void onItemClickListener(View view, String item, int position) {

    }
}
