package com.arsldev.lutluthfi.malangku.ui.splash;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.base.BaseActivity;
import com.arsldev.lutluthfi.malangku.ui.prefix.PrefixActivity;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity {

    // TODO : if application open for the first time install, go to prefix

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setUnBinder(ButterKnife.bind(this));
        setupView();
    }

    @Override
    public void setupView() {
        new Handler().postDelayed(() -> {
            startActivity(PrefixActivity.startIntent(this));
            finish();
        }, 800);
    }
}
