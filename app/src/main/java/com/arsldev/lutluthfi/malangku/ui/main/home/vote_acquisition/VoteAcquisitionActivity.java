package com.arsldev.lutluthfi.malangku.ui.main.home.vote_acquisition;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.adapter.VoteAdapter;
import com.arsldev.lutluthfi.malangku.ui.base.BaseActivity;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VoteAcquisitionActivity extends BaseActivity {

    public static final String TAG = VoteAcquisitionActivity.class.getSimpleName();

    @BindView(R.id.chart_vote)
    PieChart mChart;
    @BindView(R.id.recyclerview_vote)
    RecyclerView mRecyclerView;

    private VoteAdapter mAdapter;

    public static Intent startIntent(Context context) {
        return new Intent(context, VoteAcquisitionActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote);
        setUnBinder(ButterKnife.bind(this));
        setupView();
    }

    @Override
    public void setupView() {
        setTitle(getString(R.string.vote_acquisition));
        setupChart();
        setupRecyclerView();
    }

    private void setupChart() {
        Description description = new Description();
        description.setText("Vote for Day 1");
        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(150, "Candidate 1"));
        entries.add(new PieEntry(150, "Candidate 2"));
        entries.add(new PieEntry(200, "Candidate 3"));

        PieDataSet dataSet = new PieDataSet(entries, "Election Results Day 1");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        List<Integer> colors = new ArrayList<>();
        colors.add(getResources().getColor(R.color.colorBlue500));
        colors.add(getResources().getColor(R.color.colorGreen500));
        colors.add(getResources().getColor(R.color.colorRed500));
        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(14f);
        data.setValueTextColor(getResources().getColor(R.color.colorWhite));
        mChart.setUsePercentValues(true);
        mChart.setDescription(description);
        mChart.setData(data);
        mChart.invalidate();
    }

    private void setupRecyclerView() {
        mAdapter = new VoteAdapter();
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        layoutManager.setSmoothScrollbarEnabled(true);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return mAdapter.getItemViewType(position) == VoteAdapter.TYPE_HEAD ? 1 : 2;
            }
        });
        mRecyclerView.setHasFixedSize(true);
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        List<String> votes = new ArrayList<>();
        votes.add("0");
        votes.add("1");
        votes.add("2");
        votes.add("3");
        mAdapter.addItems(votes, 1);
        mAdapter.addItems(votes, 2);
    }
}
