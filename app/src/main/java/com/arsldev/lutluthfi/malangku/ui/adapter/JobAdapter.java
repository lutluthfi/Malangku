package com.arsldev.lutluthfi.malangku.ui.adapter;

import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.mvpboilerplate.base.PlateBaseRecyclerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobAdapter extends PlateBaseRecyclerAdapter<String, JobAdapter.JobViewHolder> {

    @NonNull
    @Override
    public JobViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new JobViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_job, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull JobViewHolder holder, int position) {
        holder.onBind(position);
    }

    class JobViewHolder extends PlateBaseRecyclerAdapter.PlateBaseViewHolder {

        @BindView(R.id.imageview_item_job_poster) ImageView mPosterImageView;
        @BindView(R.id.textview_item_job_candidate_name) TextView mCandidateNameTextView;
        @BindView(R.id.imageview_item_job_category_premium) ImageView mCategoryPremiumImageView;
        @BindView(R.id.textview_item_job_content) TextView mContentTextView;
        @BindView(R.id.textview_item_job_duration) TextView mDurationTextView;

        private final long fiveMinutes = 300000;
        private final long oneSecond = 1000;
        private CountDownTimer mTimer;

        private JobViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void onBind(int mCurrentPosition) {
            super.onBind(mCurrentPosition);
            startTimer(fiveMinutes);
        }

        private void startTimer(long duration) {
            mTimer = new CountDownTimer(duration, oneSecond) {
                @Override
                public void onTick(long millisUntilFinished) {
                    int minutes = (int) millisUntilFinished / 60000;
                    int seconds = (int) millisUntilFinished % 60000 / 1000;
                    String timeLeft = "" + minutes;
                    timeLeft += ":";
                    if (seconds < 10) timeLeft += "0";
                    timeLeft += seconds;
                    mDurationTextView.setText(timeLeft);
                }

                @Override
                public void onFinish() {

                }
            };
            mTimer.start();
        }
    }
}
