package com.arsldev.lutluthfi.malangku.ui.preference;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.adapter.CandidatePoliticalPartyAdapter;
import com.arsldev.lutluthfi.malangku.ui.main.MainActivity;
import com.arsldev.lutluthfi.mvpboilerplate.base.PlateBaseActivity;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreferenceActivity extends PlateBaseActivity {

    @BindView(R.id.viewpager_preference) ViewPager mViewPager;

    private PreferencePagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        setUnBinder(ButterKnife.bind(this));
        setupView();
    }

    @Override
    public void setupView() {
        mPagerAdapter = new PreferencePagerAdapter();
        List<String> candidates = new ArrayList<>();
        candidates.add("Arif Luthfiansyah");
        candidates.add("Muhammad Iqbal Kurniawan");
        candidates.add("Muhammad Robby D.");
        mPagerAdapter.add(candidates);
        mViewPager.setClipToPadding(false);
        mViewPager.setPadding(20, 0, 20, 0);
        mViewPager.setPageMargin(10);
        mViewPager.setAdapter(mPagerAdapter);
    }

    public void openMainActivity() {
        startActivity(MainActivity.startIntent(this));
        finish();
    }

    class PreferencePagerAdapter extends PagerAdapter implements CandidatePoliticalPartyAdapter.OnItemClickListener<String> {

        private TextView mHeadCandidateTextView;
        private RecyclerView mRecyclerView;
        private Button mSubscribeButton;
        private List<String> mCandidates;

        private CandidatePoliticalPartyAdapter mAdapter;
        private GridLayoutManager mLayoutManager;

        private PreferencePagerAdapter() {
            mCandidates = new ArrayList<>();
        }

        private void add(List<String> candidates) {
            this.mCandidates.addAll(candidates);
        }

        @SuppressWarnings("ConstantConditions")
        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater != null ? inflater.inflate(R.layout.content_preference, container, false) : null;
            mHeadCandidateTextView = view != null ? view.findViewById(R.id.textview_content_preference_head_candidate) : null;
            mRecyclerView = view != null ? view.findViewById(R.id.recyclerview_content_preference_political_party) : null;
            mSubscribeButton = view != null ? view.findViewById(R.id.button_content_preference_subscribe) : null;
            setupView(container, position);
            container.addView(view);
            return view;
        }

        private void setupView(ViewGroup container, int position) {
            mSubscribeButton.setOnClickListener(v -> {
                showMessage(mCandidates.get(position));
                openMainActivity();
            });
            mHeadCandidateTextView.setText(mCandidates.get(position));
            mAdapter = new CandidatePoliticalPartyAdapter();
            mAdapter.setOnClickListener(this);
            mLayoutManager = new GridLayoutManager(PreferenceActivity.this, 5);
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mLayoutManager.setSmoothScrollbarEnabled(true);
            mRecyclerView.setHasFixedSize(true);
            ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mAdapter);
            List<String> politicalParties = new ArrayList<>();
            politicalParties.add("Gerindra");
            politicalParties.add("PDIP");
            politicalParties.add("Golkar");
            politicalParties.add("PAN");
            mAdapter.setItems(politicalParties);
            // TODO : limit the political party of candidate, only 5
        }

        @Override
        public void onItemClickListener(View view, String item, int position) {
            showMessage(item);
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return mCandidates.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }
    }
}
