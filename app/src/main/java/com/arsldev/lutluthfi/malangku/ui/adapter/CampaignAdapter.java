package com.arsldev.lutluthfi.malangku.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.mvpboilerplate.base.PlateBaseRecyclerAdapter;
import com.arsldev.lutluthfi.mvpboilerplate.utils.ImageUtils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CampaignAdapter extends PlateBaseRecyclerAdapter<String, CampaignAdapter.CampaignViewHolder> {

    @NonNull @Override
    public CampaignViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CampaignViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_campaign, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CampaignViewHolder holder, int position) {
        holder.onBind(position);
    }

    class CampaignViewHolder extends PlateBaseRecyclerAdapter.PlateBaseViewHolder {

        @BindView(R.id.imageview_item_campaign_photo_candidate) CircleImageView mPhotoCandidateImageView;
        @BindView(R.id.textview_item_campaign_name_candidate) TextView mPoliticalPartyTextView;
        @BindView(R.id.textview_item_campaign_created_at) TextView mCreatedAtTextView;
        @BindView(R.id.textview_item_campaign_content) TextView mContentTextView;
        @BindView(R.id.recylerview_item_campaign_photo) RecyclerView mPhotoRecyclerView;

        private List<Integer> mPhotos;
        private CampaignPhotoAdapter mAdapter;

        private CampaignViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setupRecyclerView();
        }

        private void setupRecyclerView() {
            LinearLayoutManager layoutManager = new LinearLayoutManager(itemView.getContext());
            mAdapter = new CampaignPhotoAdapter();
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            layoutManager.setSmoothScrollbarEnabled(true);
            mPhotoRecyclerView.setHasFixedSize(true);
            ((SimpleItemAnimator) mPhotoRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
            mPhotoRecyclerView.setLayoutManager(layoutManager);
            mPhotoRecyclerView.setAdapter(mAdapter);
            mPhotos = new ArrayList<>();
            mPhotos.add(R.drawable.img_app_candidate);
            mPhotos.add(R.drawable.img_app_candidate);
            mPhotos.add(R.drawable.img_app_candidate);
            mPhotos.add(R.drawable.img_app_candidate);
        }

        @Override
        protected void onBind(int mCurrentPosition) {
            super.onBind(mCurrentPosition);
            mContentTextView.setText(getItem(mCurrentPosition));
            mAdapter.setItems(mPhotos);
        }
    }
}
