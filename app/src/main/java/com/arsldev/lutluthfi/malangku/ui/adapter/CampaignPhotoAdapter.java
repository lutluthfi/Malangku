package com.arsldev.lutluthfi.malangku.ui.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.mvpboilerplate.base.PlateBaseRecyclerAdapter;
import com.arsldev.lutluthfi.mvpboilerplate.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CampaignPhotoAdapter extends PlateBaseRecyclerAdapter<Integer, CampaignPhotoAdapter.CampaignPhotoViewHolder> {

    @NonNull @Override
    public CampaignPhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CampaignPhotoViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_campaign_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CampaignPhotoViewHolder holder, int position) {
        holder.onBind(position);
    }

    class CampaignPhotoViewHolder extends PlateBaseRecyclerAdapter.PlateBaseViewHolder {

        @BindView(R.id.container_item_campaign_photo) ImageView mCampaignPhotoImageView;

        private CampaignPhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void onBind(int mCurrentPosition) {
            super.onBind(mCurrentPosition);
            ImageUtils.loadImage(itemView.getContext(), getItem(mCurrentPosition), mCampaignPhotoImageView);
        }
    }
}
