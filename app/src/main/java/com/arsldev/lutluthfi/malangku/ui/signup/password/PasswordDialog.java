package com.arsldev.lutluthfi.malangku.ui.signup.password;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.signin.SignInActivity;
import com.arsldev.lutluthfi.mvpboilerplate.base.PlateBaseBottomDialog;
import com.arsldev.lutluthfi.mvpboilerplate.base.PlateBaseDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PasswordDialog extends PlateBaseBottomDialog {
    public interface InteractionListener {
        // just to show PasswordDialog
        void onPasswordDialog();
    }

    public static final String TAG = PasswordDialog.class.getSimpleName();

    @BindView(R.id.button_dialog_password_next) Button mNextButton;

    private InteractionListener mListener;

    public static PasswordDialog newInstance() {
        PasswordDialog dialog = new PasswordDialog();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        return dialog;
    }

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_password, container, false);
        setUnbinder(ButterKnife.bind(this, view));
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InteractionListener) {
            mListener = (InteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement InteractionListener");
        }
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    @Override
    protected void setupView(View view) {
        getDialog().setCanceledOnTouchOutside(false);
    }

    @OnClick(R.id.button_dialog_password_next)
    public void onButtonNextClick() {
        printLog(TAG, "onButtonNextClick: " + getBaseActivity().toString());
        startActivity(SignInActivity.startIntent(getBaseActivity()));
        getBaseActivity().finish();
    }
}
