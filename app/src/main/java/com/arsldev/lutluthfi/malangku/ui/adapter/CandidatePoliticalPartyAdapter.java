package com.arsldev.lutluthfi.malangku.ui.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.mvpboilerplate.base.PlateBaseRecyclerAdapter;
import com.arsldev.lutluthfi.mvpboilerplate.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CandidatePoliticalPartyAdapter extends PlateBaseRecyclerAdapter<String, CandidatePoliticalPartyAdapter.PoliticalPartyViewHolder> {

    @NonNull @Override
    public PoliticalPartyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PoliticalPartyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_candidate_political_party, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PoliticalPartyViewHolder holder, int position) {
        holder.onBind(position);
    }

    class PoliticalPartyViewHolder extends PlateBaseRecyclerAdapter.PlateBaseViewHolder {

        @BindView(R.id.imageview_item_candidate_political_party_photo) CircleImageView mPoliticalPartyImageView;

        private PoliticalPartyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void onBind(int mCurrentPosition) {
            super.onBind(mCurrentPosition);
            // TODO : load logo of political party
        }
    }
}
