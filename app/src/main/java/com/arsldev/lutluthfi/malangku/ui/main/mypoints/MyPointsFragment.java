package com.arsldev.lutluthfi.malangku.ui.main.mypoints;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arsldev.lutluthfi.malangku.ui.base.BaseFragment;

public class MyPointsFragment extends BaseFragment {

    public static final String TAG = MyPointsFragment.class.getSimpleName();

    public static MyPointsFragment newInstance() {
        MyPointsFragment fragment = new MyPointsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void setupView(View view) {

    }
}
