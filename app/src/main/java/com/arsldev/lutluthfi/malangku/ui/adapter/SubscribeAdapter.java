package com.arsldev.lutluthfi.malangku.ui.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.mvpboilerplate.base.PlateBaseRecyclerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubscribeAdapter extends PlateBaseRecyclerAdapter<String, SubscribeAdapter.SubscribeViewHolder> {

    @NonNull
    @Override
    public SubscribeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SubscribeViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_subscribe, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SubscribeViewHolder holder, int position) {
        holder.onBind(position);
    }

    class SubscribeViewHolder extends PlateBaseRecyclerAdapter.PlateBaseViewHolder {

        @BindView(R.id.textview_item_subscribe_content)
        TextView mContentTextView;

        private SubscribeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void onBind(int mCurrentPosition) {
            super.onBind(mCurrentPosition);
            mContentTextView.setText(getItem(mCurrentPosition));
        }
    }
}
