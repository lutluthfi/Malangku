package com.arsldev.lutluthfi.malangku.ui.main.notifications.job;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.adapter.JobAdapter;
import com.arsldev.lutluthfi.malangku.ui.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobFragment extends BaseFragment implements JobAdapter.OnItemClickListener<String>{

    public static final String TAG = JobFragment.class.getSimpleName();

    @BindView(R.id.container_job) RecyclerView mRecyclerView;

    private JobAdapter mAdapter;

    public static JobFragment newInstance() {
        JobFragment fragment = new JobFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_job, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        return view;
    }

    @Override
    public void setupView(View view) {
        mAdapter = new JobAdapter();
        mAdapter.setOnClickListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setSmoothScrollbarEnabled(true);
        mRecyclerView.setHasFixedSize(true);
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        List<String> jobs = new ArrayList<>();
        jobs.add("Kampanye akbar alun-alun malang");
        jobs.add("Kampanye akbar alun-alun malang");
        jobs.add("Kampanye akbar alun-alun malang");
        mAdapter.setItems(jobs);
    }

    @Override
    public void onItemClickListener(View view, String item, int position) {
        Toast.makeText(getBaseActivity(), item, Toast.LENGTH_SHORT).show();
    }
}
