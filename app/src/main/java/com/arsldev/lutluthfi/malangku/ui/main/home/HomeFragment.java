package com.arsldev.lutluthfi.malangku.ui.main.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.adapter.CampaignAdapter;
import com.arsldev.lutluthfi.malangku.ui.base.BaseFragment;
import com.arsldev.lutluthfi.malangku.ui.main.home.vote_acquisition.VoteAcquisitionActivity;
import com.arsldev.lutluthfi.mvpboilerplate.base.PlateBaseRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeFragment extends BaseFragment implements PlateBaseRecyclerAdapter.OnItemClickListener<String> {

    public static final String TAG = HomeFragment.class.getSimpleName();

    @BindView(R.id.recyclerview_home_campaign) RecyclerView mRecyclerView;
    @BindView(R.id.cardview_home_quick_count) CardView mQuickCountCardView;

    private CampaignAdapter mAdapter;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        return view;
    }

    @Override
    public void setupView(View view) {
        mAdapter = new CampaignAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setSmoothScrollbarEnabled(true);
        mRecyclerView.setHasFixedSize(true);
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        List<String> contents = new ArrayList<>();
        contents.add("21 Pengembang yang tergabung dalam Real Estate Indonesia (REI) DKI Jakarta menandatangani dukungan program DP 0 persen yang dicanangkan Pemprov DKI. Wakil Gubernur DKI Jakarta Sandiaga Uno menyambut dukungan itu.");
        contents.add("21 Pengembang yang tergabung dalam Real Estate Indonesia (REI) DKI Jakarta menandatangani dukungan program DP 0 persen yang dicanangkan Pemprov DKI. Wakil Gubernur DKI Jakarta Sandiaga Uno menyambut dukungan itu.");
        contents.add("21 Pengembang yang tergabung dalam Real Estate Indonesia (REI) DKI Jakarta menandatangani dukungan program DP 0 persen yang dicanangkan Pemprov DKI. Wakil Gubernur DKI Jakarta Sandiaga Uno menyambut dukungan itu.");
        contents.add("21 Pengembang yang tergabung dalam Real Estate Indonesia (REI) DKI Jakarta menandatangani dukungan program DP 0 persen yang dicanangkan Pemprov DKI. Wakil Gubernur DKI Jakarta Sandiaga Uno menyambut dukungan itu.");
        contents.add("21 Pengembang yang tergabung dalam Real Estate Indonesia (REI) DKI Jakarta menandatangani dukungan program DP 0 persen yang dicanangkan Pemprov DKI. Wakil Gubernur DKI Jakarta Sandiaga Uno menyambut dukungan itu.");
        contents.add("21 Pengembang yang tergabung dalam Real Estate Indonesia (REI) DKI Jakarta menandatangani dukungan program DP 0 persen yang dicanangkan Pemprov DKI. Wakil Gubernur DKI Jakarta Sandiaga Uno menyambut dukungan itu.");
        contents.add("21 Pengembang yang tergabung dalam Real Estate Indonesia (REI) DKI Jakarta menandatangani dukungan program DP 0 persen yang dicanangkan Pemprov DKI. Wakil Gubernur DKI Jakarta Sandiaga Uno menyambut dukungan itu.");
        mAdapter.setItems(contents);
    }

    @OnClick(R.id.cardview_home_quick_count)
    public void onQuickCountClick() {
        startActivity(VoteAcquisitionActivity.startIntent(getBaseActivity()));
    }

    @Override
    public void onItemClickListener(View view, String item, int position) {
        Toast.makeText(getBaseActivity(), item, Toast.LENGTH_SHORT).show();
    }
}
