package com.arsldev.lutluthfi.malangku.ui.prefix;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.base.BaseActivity;
import com.arsldev.lutluthfi.malangku.ui.signup.SignUpActivity;
import com.arsldev.lutluthfi.mvpboilerplate.utils.ImageUtils;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PrefixActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.viewpager_prefix) ViewPager mViewPager;
    @BindView(R.id.indicator_prefix) CirclePageIndicator mIndicator;
    @BindView(R.id.button_prefix_get_started) Button mGetStartedButton;

    private PrefixPagerAdapter mPagerAdapter;

    public static Intent startIntent(Context context) {
        return new Intent(context, PrefixActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prefix);
        setUnBinder(ButterKnife.bind(this));
        setupView();
    }

    @Override
    public void setupView() {
        mViewPager.addOnPageChangeListener(this);
        mPagerAdapter = new PrefixPagerAdapter();
        List<Integer> layouts = new ArrayList<>();
        List<Integer> images = new ArrayList<>();
        layouts.add(R.layout.content_prefix_first);
        layouts.add(R.layout.content_prefix_second);
        layouts.add(R.layout.content_prefix_third);
        layouts.add(R.layout.content_prefix_fourth);
        images.add(R.drawable.fp_alekksall_idea);
        images.add(R.drawable.fp_alekksall_super);
        images.add(R.drawable.fp_alekksall_preview);
        images.add(R.drawable.fp_alekksall_businessman_okay);
        mPagerAdapter.add(layouts, images);
        mViewPager.setClipToPadding(false);
        mViewPager.setPadding(100, 0, 100, 0);
        mViewPager.setPageMargin(50);
        mViewPager.setAdapter(mPagerAdapter);
        mIndicator.setViewPager(mViewPager);
    }

    public void onGetStartedClick(View view) {
        startActivity(SignUpActivity.startIntent(this));
        finish();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

    @Override
    public void onPageSelected(int position) {
        if (position == (mPagerAdapter.getCount() - 1)) {
            mGetStartedButton.setVisibility(View.VISIBLE);
            mIndicator.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) { }

    class PrefixPagerAdapter extends PagerAdapter {

        private CircleImageView imageView;
        private List<Integer> layouts;
        private List<Integer> images;

        private PrefixPagerAdapter() {
            layouts = new ArrayList<>();
            images = new ArrayList<>();
        }

        private void add(List<Integer> mLayouts, List<Integer> mImages) {
            this.layouts.addAll(mLayouts);
            this.images.addAll(mImages);
        }

        @SuppressWarnings("ConstantConditions") @NonNull @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater != null ? inflater.inflate(layouts.get(position), container, false) : null;
            imageView = view != null ? view.findViewById(R.id.imageview_prefix) : null;
            ImageUtils.loadImage(PrefixActivity.this, images.get(position), imageView);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return layouts.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }
    }
}
