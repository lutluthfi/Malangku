package com.arsldev.lutluthfi.malangku.ui.main.notifications;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.base.BaseFragment;
import com.arsldev.lutluthfi.malangku.ui.main.notifications.job.JobFragment;
import com.arsldev.lutluthfi.malangku.ui.main.notifications.subscribe.SubscribeFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationsFragment extends BaseFragment {

    public static final String TAG = NotificationsFragment.class.getSimpleName();

    @BindView(R.id.tabs) TabLayout mTabLayout;
    @BindView(R.id.viewpager_notification) ViewPager mViewPager;

    private NotificationPagerAdapter mPagerAdapter;

    public static NotificationsFragment newInstance() {
        NotificationsFragment fragment = new NotificationsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        return view;
    }

    @Override
    public void setupView(View view) {
        mPagerAdapter = new NotificationPagerAdapter(getChildFragmentManager());
        mPagerAdapter.add(JobFragment.newInstance(), getString(R.string.jobs));
        mPagerAdapter.add(SubscribeFragment.newInstance(), getString(R.string.subscribe));
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setSmoothScrollingEnabled(true);
        mTabLayout.setupWithViewPager(mViewPager, true);
    }

    class NotificationPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> mFragments;
        private List<String> mTitles;

        private NotificationPagerAdapter(FragmentManager fm) {
            super(fm);
            this.mFragments = new ArrayList<>();
            this.mTitles = new ArrayList<>();
        }

        private void add(Fragment fragment, String title) {
            mFragments.add(fragment);
            mTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Nullable @Override
        public CharSequence getPageTitle(int position) {
            return mTitles.get(position);
        }
    }
}
