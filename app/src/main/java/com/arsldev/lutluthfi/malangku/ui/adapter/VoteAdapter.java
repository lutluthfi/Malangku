package com.arsldev.lutluthfi.malangku.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.mvpboilerplate.base.PlateBaseRecyclerAdapter;
import com.arsldev.lutluthfi.mvpboilerplate.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VoteAdapter extends PlateBaseRecyclerAdapter<String, RecyclerView.ViewHolder> {

    public static final int TYPE_HEAD = 0;
    public static final int TYPE_BODY = 1;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEAD) {
            return new VoteHeadViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_vote_head, parent, false));
        } else if (viewType == TYPE_BODY) {
            return new VoteBodyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_vote_body, parent, false));
        }
        throw new RuntimeException("There is no type that matches the type " + viewType + " + make sure your using types correctly");

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VoteHeadViewHolder ) {
            ((VoteHeadViewHolder) holder).onBind(position);
        } else if (holder instanceof VoteBodyViewHolder) {
            ((VoteBodyViewHolder) holder).onBind(position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return isPositionHead(position) ? TYPE_HEAD : TYPE_BODY;
    }

    private boolean isPositionHead(int position) {
        return position == 0 || position % 4 == 0;
    }

    class VoteHeadViewHolder extends PlateBaseRecyclerAdapter.PlateBaseViewHolder {

        private VoteHeadViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void onBind(int mCurrentPosition) {
            super.onBind(mCurrentPosition);
        }
    }

    class VoteBodyViewHolder extends PlateBaseRecyclerAdapter.PlateBaseViewHolder {

        @BindView(R.id.imageview_item_vote_body_candidate) ImageView mCandidateImageView;

        private VoteBodyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void onBind(int mCurrentPosition) {
            super.onBind(mCurrentPosition);
            ImageUtils.loadImage(itemView.getContext(), R.drawable.img_app_candidate, mCandidateImageView);
        }
    }
}
