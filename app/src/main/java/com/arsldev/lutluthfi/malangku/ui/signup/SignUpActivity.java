package com.arsldev.lutluthfi.malangku.ui.signup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.TextView;

import com.arsldev.lutluthfi.malangku.R;
import com.arsldev.lutluthfi.malangku.ui.base.BaseActivity;
import com.arsldev.lutluthfi.malangku.ui.signup.password.PasswordDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class SignUpActivity extends BaseActivity implements PasswordDialog.InteractionListener {

    public static final String TAG = SignUpActivity.class.getSimpleName();

    @BindView(R.id.imageview_sign_up_photo) CircleImageView mPhotoImageView;
    @BindView(R.id.textview_sign_up_change_photo) TextView mChangePhotoTextView;
    @BindView(R.id.edittext_sign_up_identity_number) TextView mIdentityNumberTextView;
    @BindView(R.id.edittext_sign_up_full_name) TextView mFullNameTextView;
    @BindView(R.id.edittext_sign_up_birthday_date) TextView mBirthdayDateTextView;
    @BindView(R.id.edittext_sign_up_email) TextView mEmailTextView;
    @BindView(R.id.button_sign_up_next) Button mNextButton;

    public static Intent startIntent(Context context) {
        return new Intent(context, SignUpActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setUnBinder(ButterKnife.bind(this));
        setupView();
    }

    @Override
    public void setupView() {

    }

    @OnClick(R.id.button_sign_up_next)
    public void onButtonNextClick() {
        onPasswordDialog();
    }

    @Override
    public void onPasswordDialog() {
        // TODO: send data photo, id number, full name, birthday date, and email
        PasswordDialog.newInstance().show(getSupportFragmentManager(), PasswordDialog.TAG);
    }
}
